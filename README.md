Examples of detection:

<img src="assets/2_PANetSPPCSDarkNet53_B64SD16.jpg" width="400" /> 
<img src="assets/6_PANetSPPCSDarkNet53_B64SD16.jpg" width="400" />
<img src="assets/8_PANetSPPCSDarkNet53_B64SD16.jpg" width="400" />
<img src="assets/12_PANetSPPCSDarkNet53_B64SD16.jpg" width="400" />

![Sample video - nutria detection](assets/Nutria.mp4)

<video width="320" height="240" controls>
  <source src="assets/Nutria.mp4" type="video/mp4">
</video> 
<video src="assets/Nutria.mp4" width="320" height="240" controls>
</video> 

<!---

![1](assets/2_PANetSPPCSDarkNet53_B64SD16.jpg " ")
-->