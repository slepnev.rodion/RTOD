#!/usr/bin/python3

import os
import glob
import pandas as pd
import argparse
import shutil
import hashlib
import magic
import cv2
import math
import random
import xml.etree.ElementTree as ET
from PIL import Image
import re
import datetime
import openpyxl
from tqdm import tqdm

class Prepare(object):

	def __init__(self):

		# Construct the argument parse and parse the arguments
		ap = argparse.ArgumentParser(description='Preparation tools')

		# Argument(s) for source and destination folders
		ap.add_argument('-s', "--src", required=True, help="Source path - path to images")
		ap.add_argument('-d', "--dst", required=False, help="Destination path (source folder by default)")

		# Argument(s) for 'greyscale_images'-function - used together with '--greyscale/-gs'-flag
		greyscale = ap.add_argument_group(title='Greyscaling')
		greyscale.add_argument('-gs', "--greyscale", required=False, action="store_true", help="Greyscales all images in given folder")

		# Argument(s) for 'train_stat_yolo'-function - used together with '--train_stat_yolo/-tsy'-flag
		train_statistics = ap.add_argument_group(title='Train statistics')
		train_statistics.add_argument('-tsy', "--train_stat_yolo", required=False, type=str, help="Get training statistics for YOLO from given folder and saves it in Excel format")

		# Argument(s) for 'mAP_stat_yolo'-function - used together with '--mAP_stat_yolo/-msy'-flag
		mAP_statistics = ap.add_argument_group(title='mAP statistics')
		mAP_statistics.add_argument('-msy', "--mAP_stat_yolo", required=False, action="store_true", help="Get mAP statistics for YOLO from given folder and saves it in Excel format")
		mAP_statistics.add_argument('-rec', "--recursive", required=False, action="store_true", help="Get mAP statistics for YOLO and saves in Excel format (recursively from given folder)")

		# Argument(s) for 'resize_images'-function - used together with '--resize/-r'-flag
		resize = ap.add_argument_group(title='Resize')
		resize.add_argument('-r', "--resize", required=False, action="store_true", help="Resize all images in given folder (assign two values [any positive values; '0' for unchanged] (width and height) either for 'whpxl/width_height_pixels' or for 'whpct/width_height_percentage') [dst-folder is needed]")
		resize.add_argument('-whpxl', "--width_height_pixels", required=False, nargs='+', help="Width and height of the output image(s) in pixels")
		resize.add_argument('-whpct', "--width_height_percentage", required=False, nargs='+', help="Width and height of the output image(s) in percentage")

		# Argument(s) for 'copy_and_hash'-function - used together with '--unique_name/-un'-flag
		unique_name = ap.add_argument_group(title='Copy and hash')
		unique_name.add_argument('-un', "--unique_name", action="store_true", help="Recursively copies all images from given path and assigns unique names")

		# Argument(s) for 'convert_pascalVOC_to_yolo_format'-function - used together with '--yolo_format/-yf'-flag
		yolo_format = ap.add_argument_group(title='Convertion into YOLO-format')
		yolo_format.add_argument('-pr', "--precision", required=False, type=int, default=6, help="Output precision")
		yolo_format.add_argument('-yf', "--yolo_format", required=False, type=int, help="Converts into YOLO-format from PASCAL VOC. Set the test data in percentage (5-35).")
		yolo_format.add_argument('-cpy', "--change_path_yolo", required=False, type=str, default=False, help="Fix path for images in the corresponding train and test files ('0' by default - not to change; '1' - change to current folder;/some/custom/path' - change to this path)")

		# Argument(s) for 'get_list_of_pascalVOC_params'-function - used together with '--pascalvoc_format/-pf'-flag
		pascalvoc_format = ap.add_argument_group(title='PascalVOC parameters')
		pascalvoc_format.add_argument('-p', "--params", required=False, nargs='+', default=['path', 'xmin', 'ymin', 'xmax', 'ymax', 'name'], help="Set parameters for annotation - path, width, height, name, xmin, ymin, xmax, ymax. Default - ['path', 'xmin', 'ymin', 'xmax', 'ymax', 'name']")
		pascalvoc_format.add_argument('-hdr', "--header", required=False, action="store_true", default=False, help="No header")
		pascalvoc_format.add_argument('-cpv', "--change_path", required=False, type=str, default='n', help="Fix path for images in the corresponding XML-file ('n' by default - not to change; 'c' - change to current folder; 'e' - path == filename; '/some/custom/path' - change to this path)")
		pascalvoc_format.add_argument('-cfv', "--change_folder", required=False, type=str, default='n', help="Fix folder for images in the corresponding XML-file ('n' by default - not to change; '/some/custom/path' - change to this path)")
		pascalvoc_format.add_argument('-pf', "--pascalvoc_format", type=int, required=False, help="Set the  of test data (5-35). Three output files - labels.csv, train.csv, val.csv")
		self.args = vars(ap.parse_args())

		self.parameters = ['filename', 'path', 'width', 'height', 'name', 'xmin', 'ymin', 'xmax', 'ymax']

	def train_stat_yolo(self):

		def __check_key(element, key):

			try:
				return element[key]
			except KeyError:
				# print("No key '{0}' was found in {1}. Written '-' instead".format(key, element['file']))
				return '-'

		if self.args['dst'] is None:
			self.args['dst'] = self.args['src']

		try:

			with open(os.path.join(self.args['src'], self.args['train_stat_yolo']), 'r') as file:

				all_lines = file.readlines()
				#print(all_lines)
				amount_lines = len(all_lines)

				# Cut off first non-full chunk
				# by looking at the first match 
				# '[0-9]+ x [0-9]+' (608 x 608, 352 x 352 etc.)
				for i in range(amount_lines):
					if re.match(' [0-9]+ x [0-9]+', all_lines[i]):
						# Update size
						all_lines = all_lines[i:]
						amount_lines = len(all_lines)
						break

				# Get chunk margins
				chm = list()
				for i in range(amount_lines):
					number = re.match(' [0-9]+:', all_lines[i])
					try:
						if int(number.group()[1:-1])%10 == 0:
							chm.append(i)
					except AttributeError:
						continue
				# print(chm)

				# Get chunks by looking at 
				# '[0-9]+ x [0-9]+' (608 x 608, 352 x 352 etc.)
				# when the algorithm changes the size each 10 iterations
				chunks = list()
				chunks_ = list()
				for i in range(len(chm)-1):
					chunk = all_lines[chm[i]:chm[i+1]]
					iterations = list()
					for line in chunk:
						if re.match(' [0-9]+ x [0-9]+', line):
							iterations.append(line.strip())
					for line in chunk:
						if re.match(' [0-9]+:', line):
							iterations.append(line.strip())

					# Each resizing there must be 10 lines with 
					# iteration-info + 1 line with resizing-info 
					# itself other variants consider not correct 
					# since consdered chunk has not all info 
					# (due to wrong extraction, data was lost etc.)
					#if len(iterations) == 11:
						#chunks.append(iterations)
					#else:
					chunks.append(iterations)
					#print(iterations)
				#print(chunks)
				# Write the whole info from each chunk in a dictionary
				chunks_info = dict()
				unique_sizes = dict()
				count = 1
				for chunk in chunks:

					chunk_info = dict()
					chunk_info['avg losses'] = list()
					chunk_info['time'] = list()
					chunk_info['rate'] = list()
					chunk_info['images'] = list()
					chunk_info['iterations'] = list()

					for line in chunk:

						size  = re.match('[0-9]+ x [0-9]+', line)

						if size:
							chunk_info['size'] = line.strip()
							unique_sizes[line.strip()] = unique_sizes.get(line.strip(), 0) + 1
							continue

						avg_loss  = re.search(', [0-9]+.[0-9]+ avg loss,', line)
						if avg_loss:
							chunk_info['avg losses'].append(float(re.search("[0-9]+.[0-9]+", avg_loss.group()).group()))

						time = re.search(', [0-9]+.[0-9]+ seconds,', line)
						if time:
							chunk_info['time'].append(float(re.search("[0-9]+.[0-9]+", time.group()).group()))

						rate = re.search(', [0-9]+.[0-9]+ rate,', line)
						if rate:
							chunk_info['rate'].append(float(re.search("[0-9]+.[0-9]+", rate.group()).group()))

						images = re.search(', [0-9]+ images,', line)
						if images:
							chunk_info['images'].append(int(re.search("[0-9]+", images.group()).group()))

						number = re.search('[0-9]+:', line)
						if number:
							chunk_info['iterations'].append(int(re.search("[0-9]+", number.group()).group()))

					chunk_info['mean avg loss'] = round(sum(chunk_info['avg losses'])/len(chunk_info['avg losses']), 3)
					chunk_info['mean time'] = round(sum(chunk_info['time'])/len(chunk_info['time']), 3)
					chunk_info['mean rate'] = round(sum(chunk_info['rate'])/len(chunk_info['rate']), 6)
					chunk_info['round iteration'] = chunk_info['iterations'][0]
					chunk_info['image'] = chunk_info['images'][0]
					chunks_info["Chunk #"+str(count)] = chunk_info
					chunk_info['file'] = os.path.split(os.path.join(self.args['src'], self.args['train_stat_yolo']))[-1]
					count += 1

		except IsADirectoryError:
			print("Given path is a folder.\nProvide folder with '--src/-s'-flag and the file with '--train_stat_yolo/-tsy'-flag ")
			return

		except FileNotFoundError:
			print("No such file or directory.\nProvide folder with '--src/-s'-flag and the file with '--train_stat_yolo/-tsy'-flag ")
			return

		timestamp = datetime.datetime.now().strftime('%d%m%Y_%H%M%S')
		wb = openpyxl.Workbook()

		# Grab the active worksheet
		ws = wb.active
		ws.cell(column=1, row=1).value = "Chunk #"
		ws.cell(column=2, row=1).value = "size"
		ws.cell(column=3, row=1).value = "mean avg loss"
		ws.cell(column=4, row=1).value = "mean time"
		ws.cell(column=5, row=1).value = "mean rate"
		ws.cell(column=6, row=1).value = "iteration"
		ws.cell(column=7, row=1).value = "images"

		count = 2
		for chunk in chunks_info:
			ws.cell(column=1, row=count).value = chunk.split(' ')[1]
			ws.cell(column=2, row=count).value = __check_key(chunks_info[chunk], 'size') # chunks_info[chunk]['size']
			ws.cell(column=3, row=count).value = chunks_info[chunk]['mean avg loss']
			ws.cell(column=4, row=count).value = chunks_info[chunk]['mean time']
			ws.cell(column=5, row=count).value = chunks_info[chunk]['mean rate']
			ws.cell(column=6, row=count).value = chunks_info[chunk]['round iteration']
			ws.cell(column=7, row=count).value = chunks_info[chunk]['image']
			count += 1

		wb.save(os.path.join(self.args['dst'], "train_output_{0}.xlsx".format(timestamp)))

	def mAP_stat_yolo(self):

		def __recursive_listing(src):
			items = os.listdir(src)
			for item in items:
				folder_or_file = os.path.join(src, item)
				if os.path.isdir(folder_or_file):
					if '_mAP' in folder_or_file:
						mAP_folders.append(folder_or_file)
					else:
						__recursive_listing(os.path.join(src, item))

		def __check_key(element, key):

			try:
				return element[key]
			except KeyError:
				print("No key '{0}' was found in {1}. Written '-' instead".format(key, element['file']))
				return '-'

		def __get_mAP(folder):

			files = glob.glob('{0}/*.txt'.format(folder))

			# Organize files in correct order
			ordered_files = list()
			try:
				order = [int(re.search("[0-9]+", os.path.split(file)[-1]).group()) for file in files]
				order.sort()
				for o in order:
					for file in files:
						if o == int(re.search("[0-9]+", os.path.split(file)[-1]).group()):
							ordered_files.append(file)
							break
			except AttributeError:
				files.sort()

			files = ordered_files.copy()
			mAP_info = list()
			for file in files:
				try:
					with open(file, 'r') as f:
						all_lines = f.readlines()
						file_len = len(all_lines)
						chunk_info = dict()
						for line in range(file_len):
							mAP50_0 = re.search("IoU threshold = 50 %, used Area-Under-Curve for each unique Recall", all_lines[line])
							mAP75_0 = re.search("IoU threshold = 75 %, used Area-Under-Curve for each unique Recall", all_lines[line])
							mAP50_11 = re.search("IoU threshold = 50 %, used 11 Recall-points", all_lines[line])
							mAP75_11 = re.search("IoU threshold = 75 %, used 11 Recall-points", all_lines[line])
							mAP50_101 = re.search("IoU threshold = 50 %, used 101 Recall-points", all_lines[line])
							mAP75_101 = re.search("IoU threshold = 75 %, used 101 Recall-points", all_lines[line])
							if mAP50_0:
								chunk_info["mAP50 0"] = float(re.findall("[0-9]+.[0-9]+", all_lines[line+1])[1])
							elif mAP75_0:
								chunk_info["mAP75 0"] = float(re.findall("[0-9]+.[0-9]+", all_lines[line+1])[1])
							elif mAP50_11:
								chunk_info["mAP50 11"] = float(re.findall("[0-9]+.[0-9]+", all_lines[line+1])[1])
							elif mAP75_11:
								chunk_info["mAP75 11"] = float(re.findall("[0-9]+.[0-9]+", all_lines[line+1])[1])
							elif mAP50_101:
								chunk_info["mAP50 101"] = float(re.findall("[0-9]+.[0-9]+", all_lines[line+1])[1])
							elif mAP75_101:
								chunk_info["mAP75 101"] = float(re.findall("[0-9]+.[0-9]+", all_lines[line+1])[1])
					try:
						chunk_info["file"] = os.path.split(file)[-1]
						chunk_info["iteration"] = int(re.search("[0-9]+", os.path.split(file)[-1]).group())
					except AttributeError:
						print("Filename is wrong. Must be 'outputX' where X is iteration number. Fullname is written instead")
						chunk_info["iteration"] = os.path.split(file)[-1]
					mAP_info.append(chunk_info)
				except UnicodeDecodeError:
					continue

			# timestamp = datetime.datetime.now().strftime('%d%m%Y_%H%M%S')
			wb = openpyxl.Workbook()

			# Grab the active worksheet
			ws = wb.active
			ws.cell(column=1, row=1).value = "iteration"
			ws.cell(column=2, row=1).value = "mAP50 0"
			ws.cell(column=3, row=1).value = "mAP75 0"
			ws.cell(column=4, row=1).value = "mAP50 11"
			ws.cell(column=5, row=1).value = "mAP75 11"
			ws.cell(column=6, row=1).value = "mAP50 101"
			ws.cell(column=7, row=1).value = "mAP75 101"

			count = 2
			for iteration in mAP_info:
				ws.cell(column=1, row=count).value = iteration['iteration']
				ws.cell(column=2, row=count).value = __check_key(iteration, 'mAP50 0')
				ws.cell(column=3, row=count).value = __check_key(iteration, 'mAP75 0')
				ws.cell(column=4, row=count).value = __check_key(iteration, 'mAP50 11')
				ws.cell(column=5, row=count).value = __check_key(iteration, 'mAP75 11')
				ws.cell(column=6, row=count).value = __check_key(iteration, 'mAP50 101')
				ws.cell(column=7, row=count).value = __check_key(iteration, 'mAP75 101')
				count += 1

			# wb.save(os.path.join(self.args['dst'], "mAP_output_{0}.xlsx".format(timestamp)))
			wb.save(os.path.join(self.args['dst'], "{0}.xlsx".format(os.path.split(folder)[-1])))

		if self.args['dst'] is None:
			self.args['dst'] = self.args['src']

		if self.args['mAP_stat_yolo_recursive']:

			mAP_folders = list()
			__recursive_listing(self.args['src'])
			# print(mAP_folders)
			for folder in mAP_folders:
				__get_mAP(folder)

		else:
			__get_mAP(self.args['src'])

	# Resizes all images in a given folder according to input sizes
	def resize_images(self):

		def resize_in_pixels(pixel_sizes):
			assigned_width = int(pixel_sizes[0])
			assigned_height = int(pixel_sizes[1])

			files = glob.glob('{0}/*.*'.format(self.args['src']))
			amount_of_files = len(files)
			for i in tqdm(range(amount_of_files)):

				try:
					img = Image.open(files[i])
				except OSError:
					continue

				if assigned_width == 0:
					width = img.size[0]
				else:
					width = assigned_width

				if assigned_height == 0:
					height = img.size[1]
				else:
					height = assigned_height

				resized_img = img.resize((width, height), Image.ANTIALIAS)

				try:
					resized_img.save(os.path.join(self.args['dst'], os.path.split(files[i])[-1]), "")
				except OSError:
					print("Cannot resize {0}".format(file))

		def resize_in_percentage(percentage_sizes):

			pct_width = int(percentage_sizes[0])
			pct_height = int(percentage_sizes[1])
			files = glob.glob('{0}/*.*'.format(self.args['src']))
			amount_of_files = len(files)
			for i in tqdm(range(amount_of_files)):

				try:
					img = Image.open(files[i])
				except OSError:
					continue

				if pct_width == 0:
					width = img.size[0]
				else:
					width = int(img.size[0]*pct_width/100)

				if pct_height == 0:
					height = img.size[1]
				else:
					height = int(img.size[1]*pct_height/100)

				resized_img = img.resize((width, height), Image.ANTIALIAS)

				try:
					resized_img.save(os.path.join(self.args['dst'], os.path.split(files[i])[-1]), "")
				except OSError:
					print("Cannot resize {0}".format(file))

		if not(self.args['width_height_pixels'] is None) and not(self.args['width_height_percentage'] is None):
			print("Assign two values for either 'whpxl/width_height_pixels' or 'whpct/width_height_percentage' [not both]")
			return

		elif not(self.args['width_height_pixels'] is None):

			if len(self.args['width_height_pixels']) != 2:
				print("There must be assigned two values")
				return
			else:
				for size in self.args['width_height_pixels']:
					if int(size) < 0:
						print("Values must be only poisitive")
						return
				resize_in_pixels(self.args['width_height_pixels'])

		elif not(self.args['width_height_percentage'] is None): 

			if len(self.args['width_height_percentage']) != 2:
				print("There must be assigned two values")
				return
			else:
				for size in self.args['width_height_percentage']:
					if int(size) < 0:
						print("Values must be only poisitive")
						return
				resize_in_percentage(self.args['width_height_percentage'])

	# Recursively copies everything from given folder and calculates their hashes and 
	# assign it as thier names (to avoid same files with different names)
	def copy_and_hash(self):

		def recursive_copy(src, dst):

			items = os.listdir(src)
			for item in items:
				if os.path.isfile(os.path.join(src, item)):
					with open(os.path.join(src, item), "rb") as f:
						file_in_bytes = f.read()
						readable_hash = hashlib.sha256(file_in_bytes).hexdigest()
						try:
							shutil.copyfile(os.path.join(src, item), os.path.join(dst, '{0}{1}'.format(readable_hash, os.path.splitext(item)[1])))
						except shutil.SameFileError:
							pass
				elif os.path.isdir(os.path.join(src, item)):
					recursive_copy(os.path.join(src, item), dst)

		if not self.args['dst']:
			self.args['dst'] = self.args['src']
		recursive_copy(self.args['src'], self.args['dst'])

	# Internal function (protected) is used for 'convert_pascalVOC_to_yolo_format'-function
	def _get_annotations(self, xml_file):

		read = open(xml_file, 'r')
		whole_text = ''.join(read.readlines())

		values = list()
		main_chunk = dict()
		obj_chunk = dict()
		main_part = self.parameters[:4]
		obj_part = self.parameters[4:]

		# Get the information for the image (filename, path, width, height)
		for i in main_part:
			main_chunk[i] = whole_text[whole_text.find('<'+i+'>')+len('<'+i+'>'):whole_text.find('</'+i+'>')]

		while True:
			# Get the information for one specific object (name, xmin, ymin, xmax, ymax)
			obj = whole_text[whole_text.find('<object>')+len('<object>'):whole_text.find('</object>')]
			for i in obj_part:
				obj_chunk[i] = whole_text[whole_text.find('<'+i+'>')+len('<'+i+'>'):whole_text.find('</'+i+'>')]

			# Merge both chunks
			values.append({**main_chunk, **obj_chunk})

			# Move to the next object
			whole_text = whole_text[whole_text.find('</object>')+len('</object>'):]

			# retrun values if no more object were found
			if whole_text.find('</object>') == -1:
				return values

	def convert_pascalVOC_to_yolo_format(self):

		if self.args['dst'] is None:
			self.args['dst'] = self.args['src']

		if not(os.path.exists(os.path.join(self.args['dst'], 'cfg'))):
			os.mkdir(os.path.join(self.args['dst'], 'cfg'))

		class_names_file = open(os.path.join(self.args['dst'], 'cfg', 'obj.names'), 'w')
		class_names = dict()
		class_counter = 0
		for xml_file in glob.glob(self.args['src'] + '/*.xml'):
			values = self._get_annotations(xml_file)
			with open(os.path.join(self.args['dst'],'{0}.txt'.format(os.path.basename(os.path.splitext(xml_file)[0]))), 'w') as file:
				for value in values:

					# If class name is new then add it to the dictionary and write to *.names
					if not(value['name'] in class_names):
						class_names_file.write(value['name'])
						class_names_file.write("\n")
						class_names[value['name']] = class_counter
						class_counter += 1

					xmin = float(value['xmin'])
					xmax = float(value['xmax'])
					ymin = float(value['ymin'])
					ymax = float(value['ymax'])
					w = float(value['width'])
					h = float(value['height'])

					try:
						f_value = [class_names[value['name']],
						float("{0:.{1}f}".format(((xmax-xmin)/2+xmin)/w, self.args['precision'])),
						float("{0:.{1}f}".format(((ymax-ymin)/2+ymin)/h, self.args['precision'])),
						float("{0:.{1}f}".format((xmax-xmin)/w, self.args['precision'])),
						float("{0:.{1}f}".format((ymax-ymin)/h, self.args['precision']))]
					except KeyError:
						print('No such name as {0} for file {1}'.format(value['name'], value['filename']))
						os.remove(os.path.join(self.args['dst'],'{0}.txt'.format(os.path.splitext(value['filename'])[0])))
						continue
					except ZeroDivisionError:
						print("Wrong parameters in {0}. Skipped".format(value['filename']))

					file.write(str(f_value).replace(', ', ' ')[1:-1])
					file.write('\n')

		class_names_file.close()

		if not(os.path.exists(os.path.join(self.args['dst'], 'cfg', 'backup'))):
			os.mkdir(os.path.join(self.args['dst'], 'cfg', 'backup'))

		with open(os.path.join(self.args['dst'], 'cfg', 'obj.data'), 'w') as file:
			file.write('classes={0}\n'.format(class_counter))
			file.write('train={0}\n'.format(os.path.join(self.args['src'],'train.txt')))
			file.write('valid={0}\n'.format(os.path.join(self.args['src'],'valid.txt')))
			file.write('names={0}\n'.format(os.path.join(self.args['dst'], 'cfg', 'obj.names')))
			file.write('backup={0}/\n'.format(os.path.join(self.args['dst'], 'cfg', 'backup')))

		self._split_into_train_and_test_sets()

	# Check image extensions (must be either of type JPEG (*.jpg, *.jpeg) or PNG (*.png)
	def _check_image_extension(self, file):
		try:
			if 'PNG' in magic.from_file(file) or 'JPEG' in magic.from_file(file):
				return True
			return False
		except IsADirectoryError:
			return False

	def greyscale_images(self):
		files = os.listdir(self.args['src'])
		images = list()
		for i in files:
			if not self._check_image_extension(os.path.join(self.args['src'], i)):
				continue
			images.append(i)

		for i in images:
			image = cv2.imread(os.path.join(self.args['src'], i))
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			cv2.imwrite(os.path.join(self.args['dst'], i), gray)

	# If 'cpv/change_path'-flag (used together with 'pascalvoc_format/pvl'-flag) is True, 
	# the following function changes the paths in XML-files
	# to paths where these XML's currently are
	def _change_path(self, xml_files):

		for xml_file in xml_files:
			tree = ET.parse(xml_file)
			root = tree.getroot()
			filename = root.find('filename')
			path = root.find('path')
			if self.args['change_path'] == 'c':
				path.text = os.path.join(self.args['src'], filename.text)
			elif self.args['change_path'] == 'e':
				path.text = filename.text
			else:
				path.text = os.path.join(self.args['change_path'], filename.text)
			tree.write(os.path.join(self.args['dst'], os.path.split(xml_file)[1]))

	def _change_folder(self, xml_files):

		for xml_file in xml_files:
			tree = ET.parse(xml_file)
			root = tree.getroot()
			folder = root.find('folder')
			folder.text = self.args['change_folder']
			tree.write(os.path.join(self.args['dst'], os.path.split(xml_file)[1]))

	def _split_into_train_and_test_sets(self):

		# Percentage of images to be used for the test set
		if self.args['yolo_format'] < 5 or self.args['yolo_format'] > 35:
			print('Percentage should be no more than 35% and no less than 5%')
			return

		# Populate train.txt and test.txt  
		jpg_images = glob.glob(os.path.join(self.args['src'], '*.jpg'))
		jpeg_images = glob.glob(os.path.join(self.args['src'], '*.jpeg'))
		png_images = glob.glob(os.path.join(self.args['src'], '*.png'))
		all_images = jpg_images.copy() + jpeg_images.copy() + png_images.copy()

		# Pick up filenames randomly out of given list with all image names
		random.seed(a=random.random(),version=2)
		random.shuffle(all_images)
		b = round(self.args['yolo_format']*len(all_images)/100)
		train_images = all_images[b:]
		test_images = all_images[:b]

		if self.args['change_path_yolo'] is False:

			with open(os.path.join(self.args['dst'], 'train.txt'), 'w') as file:
				file.write("\n".join(train_images))

			with open(os.path.join(self.args['dst'], 'valid.txt'), 'w') as file:
				file.write("\n".join(test_images))
		else:
			with open(os.path.join(self.args['dst'], 'train.txt'), 'w') as file:
				for path in train_images:
					file.write(os.path.join(self.args['change_path_yolo'], os.path.split(path)[1])+'\n')

			with open(os.path.join(self.args['dst'], 'valid.txt'), 'w') as file:
				for path in test_images:
					file.write(os.path.join(self.args['change_path_yolo'], os.path.split(path)[1])+'\n')

	def get_list_of_pascalVOC_params(self):

		if self.args['dst'] is None:
			self.args['dst'] = self.args['src']

		xml_list = []
		if not(self.args['params'] is None):
			for i in self.args['params']:
				if not(i in self.parameters):
					print('Such parameter as {0} does not exist'.format(i))
					return None

		xml_files = glob.glob(self.args['src'] + '/*.xml')

		if xml_files != list():

			if self.args['change_path'] != 'n':
				self._change_path(xml_files)
			if self.args['change_folder'] != 'n':
				self._change_folder(xml_files)

			for xml_file in xml_files:
				values = self._get_annotations(xml_file)
				for value in values:
					f_value = [value[i] for i in self.args['params']]
				xml_list.append(f_value)
			xml_df_all = pd.DataFrame(xml_list, columns=self.args['params'])
			random.shuffle(xml_list)

			b = round(self.args['pascalvoc_format']*len(xml_list)/100)

			xml_df_train = pd.DataFrame(xml_list[b:], columns=self.args['params'])
			xml_df_val = pd.DataFrame(xml_list[:b], columns=self.args['params'])

			# With header or not
			d = {'trainval_pvoc':xml_df_all, 'train_pvoc':xml_df_train, 'val_pvoc':xml_df_val}
			for i in d:

				try:
					os.remove(os.path.join(self.args['dst'], i+'.csv'))
					os.remove(os.path.join(self.args['dst'], i+'.txt'))
				except FileNotFoundError:
					pass
				if self.args['header']:
					d[i].to_csv(os.path.join(self.args['dst'], i+'.csv'), index=None)
				else:
					d[i].to_csv(os.path.join(self.args['dst'], i+'.csv'), index=None, header=False)
				# Copy *.csv and save as *.txt
				try:
					shutil.copyfile(os.path.join(self.args['dst'], i+'.csv'), os.path.join(self.args['dst'], i+'.txt'))
				except shutil.SameFileError:
					pass

		else:
			print('No XML-files were found')

	def _check_for_dst_path(self):
		if self.args['dst'] is None:
			print("For the given option dst-folder is needed")
			return False
		elif self.args['dst'] == self.args['src']:
			print("dst-folder must be not the same as src-folder")
			return False
		return True

	def start(self):

		if not os.path.exists(self.args['src']):
			print("Given path does not exist")
			return
		if not os.path.isdir(self.args['src']):
			print("The source path is not a folder")
			return

		checksum = \
		int(self.args['resize']) + \
		int(self.args['greyscale']) + \
		int(type(self.args['yolo_format'])==type(0)) + \
		int(type(self.args['pascalvoc_format'])==type(0)) + \
		int(self.args['unique_name']) + \
		int(self.args['mAP_stat_yolo']) + \
		int(type(self.args['train_stat_yolo'])==type(''))

		if checksum == 0:
			print("There must be called at least one flag together with -s/--src (and -d/--dst)")
		elif checksum != 1:
			print("There must be called only one flag together with -s/--src (and -d/--dst)")
		else:
			if self.args['greyscale']:
				if self._check_for_dst_path():
					self.greyscale_images() 
			elif type(self.args['yolo_format'])==type(0):
				self.convert_pascalVOC_to_yolo_format()
			elif type(self.args['pascalvoc_format'])==type(0):
				self.get_list_of_pascalVOC_params()
			elif self.args['unique_name']:
				self.copy_and_hash()
			elif self.args['resize']:
				if self._check_for_dst_path():
					self.resize_images()
			elif self.args['mAP_stat_yolo']:
				self.mAP_stat_yolo()
			elif type(self.args['train_stat_yolo'])==type(''):
				self.train_stat_yolo() 

def main():

		instance = Prepare()
		instance.start()

if __name__ == '__main__':
	main()
