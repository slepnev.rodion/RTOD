#!/usr/bin/python3

# import the necessary packages
import argparse
import cv2
import datetime
import glob
import numpy as np
import os
import time
import magic
import imutils
import random
from tqdm import tqdm
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils.video import FPS
from multiprocessing import Process
from multiprocessing import Queue
import pathlib

class Test(object):

	def __init__(self):
			
		# Construct the argument parse and parse the arguments
		ap = argparse.ArgumentParser(description='Testing tools')

		ap.add_argument('-d', '--darknet', required=False, help="Path to darknet(-based) directory (must contain  *.cfg, *.names (list of objects to detect), *.weights - each one only once)")
		ap.add_argument('-cfd', '--confidence', type=float, default=0.5, help="Minimum probability to filter weak detections")
		ap.add_argument('-t', '--threshold', type=float, default=0.3, help="Threshold when applying non-maxima suppression")
		ap.add_argument('-ve', '--video_extension', required=False, default='mp4', type=str, help="Video extension ('avi' by default)")
		ap.add_argument('-cdc', '--codec', required=False, default='mp4v', type=str, help="Code of codec used to compress the frames. For more information refer to https://www.fourcc.org/codecs.php")
		ap.add_argument('-c', '--caffe', required=False, help="Base path to caffe(-based) directory (must contain  *.caffemodel, *.prototxt (deploy), *.names (list of objects to detect) - each one only once)")
		ap.add_argument("-g", "--gpu", action='store_true', default=False, help="Use CUDA GPU")
		ap.add_argument("-sf", "--save_frame", action='store_true', default=False, help="If true then save current frame")

		# Image source
		image_src = ap.add_argument_group(title='Image source')
		image_src.add_argument("-is", "--image_src", required=False, help="Folder with image(s)")

		# Video source
		video_src = ap.add_argument_group(title='Video source')
		video_src.add_argument("-vs", "--video_src", required=False, help="Path to video")
		video_src.add_argument('-fps', '--frames_per_second', required=False, type=int, default=0, help="Frames per second (20 for videocapture and 30 for videoinput by default)")

		# Videocapture
		video_cap = ap.add_argument_group(title='Videocapture')
		video_cap.add_argument("-vc", "--video_cap", type=int, required=False, help="Process video from camera. Set the camera number.")
		video_cap.add_argument('-o', '--output', required=False, action='store_true', default=False, help="Video output (False by default); reduces FPS")
		video_cap.add_argument('-mp', '--multiprocessing', action='store_true', required=False, help="Run videocapture using multiprocessing")

		self.args = vars(ap.parse_args())

		# tools_folder = (pathlib.Path(__file__) / ".." ).resolve()
		main_folder = (pathlib.Path(__file__) / ".." / "..").resolve()
		results_folder = os.path.join(main_folder, 'Results')

		if not os.path.exists(results_folder):
			os.mkdir(results_folder)

		while True:
			try:
				# Create unique folder for each 'Session'
				self.timestamp = datetime.datetime.now().strftime('%d%m%Y_%H%M%S')
				self.path = os.path.join(results_folder, 'Session_{0}'.format(self.timestamp))
				os.mkdir(self.path)
				break
			except FileExistsError:
				continue

	def classify_frame_caffe(self):

		while True:

			# Check to see if there is a frame in our input queue grab the frame 
			# from the input queue, resize it, and construct a blob from it
			if not self.inputQueue.empty():

				frame = self.inputQueue.get()
				# frame = cv2.resize(frame, (300, 300))

				# blob = cv2.dnn.blobFromImage(frame, 0.007843, (300, 300), 127.5)
				scalefactor = 0.004 # 1/255.0
				blob = cv2.dnn.blobFromImage(frame, scalefactor, (416, 416), swapRB=True, crop=False)

				# Set the blob as input to our deep learning object
				# detector and obtain the detections
				self.net.setInput(blob)
				detections = self.net.forward()

				# Write the detections to the output queue
				self.outputQueue.put(detections)


	def classify_frame_darknet(self):

		while True:

			# Check to see if there is a frame in our input queue grab the frame 
			# from the input queue, resize it, and construct a blob from it
			if not self.inputQueue.empty():

				frame = self.inputQueue.get()
				# frame = cv2.resize(frame, (300, 300))

				# Construct a blob from the input image and then perform a 
				# forward pass of the Darknet(-based) object detector,
				# giving us our bounding boxes and associated probabilities
				scalefactor = 0.004 # 1/255.0
				blob = cv2.dnn.blobFromImage(frame, scalefactor, (416, 416), swapRB=True, crop=False)
				
				# Set the blob as input to our deep learning 
				# object detector and obtain the detections
				self.net.setInput(blob)
				layerOutputs = self.net.forward(self.ln)

				# Write the detections to the output queue
				self.outputQueue.put(layerOutputs)			

	def _check_file(self, extension, model_type, read=False):
			
		if len(glob.glob(os.path.join(model_type, extension))) != 1:
			print("There are either no or more than one file as '{0}'".format(extension))
			return
		else:
			if read:
				with open(glob.glob(os.path.join(model_type, extension))[0], 'r') as file:
					file = ''.join(file.readlines()).strip().split('\n')
			else:
				file = glob.glob(os.path.join(model_type, extension))[0]
		return file

	def process_frame_caffe(self, frame):

		detections = None
		(H, W) = frame.shape[:2]

		if self.inputQueue.empty():
			self.inputQueue.put(frame)
		if not self.outputQueue.empty():
			detections = self.outputQueue.get()

		# Frame parameters
		white_rect = [int(255),int(255),int(255)]
		black_text = [int(0),int(0),int(0)]
		text_adjust = int(H/150)
		height_frame = int(H/300)
		white_rect_adjust = int(H/25)
		thickness = round(H/1000, 1)
		
		# Pass the blob through the network and  
		# obtain the detections and predictions
		if detections is not None:

			# Loop over the detections
			for i in np.arange(0, detections.shape[2]):
				
				# Extract the confidence (i.e., probability) 
				# associated with the prediction
				confidence = detections[0, 0, i, 2]
				
				# Filter out weak detections by ensuring the `confidence` is
				# greater than the minimum confidence
				if confidence > self.args["confidence"]:
					
					# Extract the index of the class label from the 'detections',
					# then compute the (x, y)-coordinates of the bounding box for the object
					idx = int(detections[0, 0, i, 1])
					box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
					(xmin, ymin, xmax, ymax) = box.astype("int")
					
					try:
						color = [int(c) for c in self.colors[idx]]
					except IndexError:
						print("Unspecified class (name) is detected. Check the *.names")
						return

					# Draw the prediction on the frame
					# Draw a bounding box rectangle and label on the image
					cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), color, height_frame)
					cv2.rectangle(frame, (xmin, ymin - white_rect_adjust), (xmax, ymin), white_rect, cv2.FILLED)
					text = "{}: {:.4f}".format(self.labels[idx], confidence*100)
					cv2.putText(frame, text, (xmin, ymin - text_adjust), cv2.FONT_HERSHEY_SIMPLEX, thickness, black_text, height_frame)
					print((xmin, ymin), (xmax, ymax), color, height_frame)

		# check to see if the output frame 
		# should be displayed to our screen
		return frame, 0

	def process_image_darknet(self, frame):

		# Construct a blob from the input image and then perform a 
		# forward pass of the Darknet(-based) object detector,
		# giving us our bounding boxes and associated probabilities
		scalefactor = 0.004 # 1/255.0
		blob = cv2.dnn.blobFromImage(frame, scalefactor, (416, 416), swapRB=True, crop=False)

		# Set the blob as input to our deep learning 
		# object detector and obtain the detections
		self.net.setInput(blob)
		layerOutputs = self.net.forward(self.ln)

		return self.process_frame_darknet(frame, layerOutputs)


	def process_frame_darknet(self, frame, layerOutputs):

		(H, W) = frame.shape[:2]
		# Frame parameters
		white_rect = [int(255),int(255),int(255)]
		black_text = [int(0),int(0),int(0)]
		text_adjust = int(H/150)
		height_frame = int(H/300)
		white_rect_adjust = int(H/25)
		thickness = round(H/1000, 1)

		# Initialize our lists of detected bounding boxes, 
		# confidences, and class IDs, respectively
		boxes = []
		confidences = []
		classIDs = []

		if layerOutputs is not None:
                        
			# Loop over each of the layer outputs
			for output in layerOutputs:
				
				# Loop over each of the detections
				for detection in output:
						
					# Extract the class ID and confidence (i.e., probability) of the current object detection
					scores = detection[5:]
					classID = np.argmax(scores)
					confidence = scores[classID]
	 
					# Filter out weak predictions by ensuring the detected 
					# probability is greater than the minimum probability
					if confidence > self.args['confidence']:
							
						# Scale the bounding box coordinates back relative to the size of the image, keeping in mind that Darknet(-based) actually
						# returns the center (x, y)-coordinates of the bounding box followed by the boxes' width and height
						box = detection[0:4] * np.array([W, H, W, H])
						(centerX, centerY, width, height) = box.astype('int')

						# Use the center (x, y)-coordinates to derive the top and left corner of the bounding box
						x = int(centerX - (width/2))
						y = int(centerY - (height/2))

						# Update our list of bounding box coordinates, confidences and class IDs
						boxes.append([x, y, int(width), int(height)])
						confidences.append(float(confidence))
						classIDs.append(classID)
			
		# Apply non-maxima suppression to suppress weak, overlapping bounding boxes
		idxs = cv2.dnn.NMSBoxes(boxes, confidences, self.args['confidence'], self.args['threshold'])
		det_rate = list()
		if confidences == list():
			self.stat.write("Nothing: {0}\n".format(0))

		# Ensure at least one detection exists
		if len(idxs) > 0:

			# Loop over the indexes we are keeping
			for i in idxs.flatten():

				det_rate.append(confidences[i])
				
				# Extract the bounding box coordinates
				(x, y) = (boxes[i][0], boxes[i][1])

				# If negative then 0 or if bigger than frame size then frame size
				x = min(W, max(0, x))
				y = min(H, max(0, y))
				(w, h) = (boxes[i][2], boxes[i][3])
				w = min(W, max(0, w))
				h = min(H, max(0, h))

				# Draw a bounding box rectangle and label on the image
				try:
					color = [int(c) for c in self.colors[classIDs[i]]]
					# anti_color = [int(c)-255*(-1) for c in self.colors[classIDs[i]]]
				except IndexError:
					print("Unspecified class (name) is detected. Check the *.names")
					return

				cv2.rectangle(frame, (x, y), (x + w, y + h), color, height_frame)
				text = "{}: {:.4f}".format(self.labels[classIDs[i]], confidences[i])
				self.stat.write(text)
				self.stat.write('\n')
				cv2.rectangle(frame, (x, y - white_rect_adjust), (x + w, y), white_rect, cv2.FILLED)
				cv2.putText(frame, text, (x, y - text_adjust), cv2.FONT_HERSHEY_SIMPLEX, thickness, black_text, height_frame)

			if self.args['save_frame']:
				cv2.imwrite(os.path.join(self.path, datetime.datetime.now().strftime('%d%m%Y_%H%M%S')+'.jpg'), frame)

		return frame, det_rate


	def process_frame_darknet_mp(self, frame):
		
		# Load our input image and grab its spatial dimensions
		layerOutputs = None
		if self.inputQueue.empty():
			self.inputQueue.put(frame)
		if not self.outputQueue.empty():
			layerOutputs = self.outputQueue.get()

		return self.process_frame_darknet(frame, layerOutputs)
	
	# Record the captured video
	def save_video(self, vs):

		output = os.path.join(self.path, 'vc_output_{0}.{1}'.format(self.timestamp, self.args['video_extension']))

		frame = vs.read()
		(H, W) = frame.shape[:2]

		# Define the codec and create VideoWriter object
		fourcc = cv2.VideoWriter_fourcc(*self.args['codec'])
		out = cv2.VideoWriter(output, fourcc, self.args['frames_per_second'], (W,H))

		return out
					
	# Write statistics to the file
	def save_statistics(self):
					
		stat = open(os.path.join(self.path, 'stat_{0}.txt'.format(self.timestamp)), 'w')

		if self.args['darknet']:
			stat.write("Darknet(-based) model - {0}\n".format(self.args['darknet']))
		elif self.args['caffe']:
			stat.write("Caffe(-based) model - {0}\n".format(self.args['caffe']))
		if self.args['image_src']:
			stat.write("Image source - {0}\n".format(self.args['image_src']))
		elif self.args['video_src']:
			stat.write("Video source - {0}\n".format(self.args['video_src']))
		elif self.args['video_cap']:
			stat.write("Videocapture - {0}\n".format(self.args['video_cap']))
				
		stat.write("Results are saved in {0}\n".format(self.path))
		stat.write("Confidence (minimum probability to filter weak detections) - {0}\n".format(self.args['confidence']))
		stat.write("Threshold (threshold when applying non-maxima suppression) - {0}\n\n".format(self.args['threshold']))

		return stat

	def get_cfg(self):

		if not(self.args['caffe'] is None):

			if os.path.exists(self.args['caffe']):
				self.labels = self._check_file('*.names', self.args['caffe'], read=True)
				cfg  = self._check_file('*.prototxt', self.args['caffe'])
				weights = self._check_file('*.caffemodel', self.args['caffe'])
			else:
				print("The following path does not exist:\n{0}".format(self.args['caffe']))
				return (None, None)

		elif not(self.args['darknet'] is None):

			if os.path.exists(self.args['darknet']):
				self.labels = self._check_file('*.names', self.args['darknet'], read=True)
				cfg  = self._check_file('*.cfg', self.args['darknet'])
				weights = self._check_file('*.weights', self.args['darknet'])    
			else:
				print("The following path does not exist:\n{0}".format(self.args['darknet']))
				return (None, None)

		return cfg, weights

	def get_frcnn_cfg(self):
			
		# Derive the files from the Darknet(-based) directory if any
		if os.path.exists(self.args['frcnn']):
			cfg = self._check_file('*.pb', self.args['frcnn'])
			weights  = self._check_file('*.pbtxt', self.args['frcnn'])
		else:
			print('Given path does not exist')
			return
		return cfg, weights

	def define_net_colors_labels_layers(self):

		# Initialize a list of colors to represent each possible class label
		np.random.seed(random.seed(a=random.random(),version=2))
		cfg, weights = self.get_cfg()
		if None in [self.labels, cfg, weights]:
			return

		# Load trained Darknet(-based) object detector
		if self.args['darknet']:
			self.net = cv2.dnn.readNetFromDarknet(cfg, weights)

			# Determine only the *output* layer names that we need from Darknet(-based)
			self.ln = self.net.getLayerNames()
			self.ln = [self.ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]
			
		# Load trained Caffe(-based) object detector
		elif self.args['caffe']:
			self.net = cv2.dnn.readNet(cfg, weights)

		self.inputQueue = Queue(maxsize=1)
		self.outputQueue = Queue(maxsize=1)

		self.colors = np.random.randint(0, 255, size=(len(self.labels), 3), dtype='uint8')

		return True

	def video_processing(self):

		# Initialize the video stream, pointer to output video file and frame dimensions
		vs = cv2.VideoCapture(self.args["video_src"])
		if not vs.isOpened():
			print("Videofile either does not exist or corrupted")
			return
		if self.args['frames_per_second'] == 0:
			self.args['frames_per_second'] = 30

		prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() else cv2.CAP_PROP_FRAME_COUNT

		# Determine the total number of frames in the video file
		total = int(vs.get(prop))
		print("[INFO] {0} total frames in video".format(total))
		self.stat.write("[INFO] {0} frames in video:\n".format(total))

		# Initialize frame-writer
		ret, frame = vs.read()
		if ret:
			(H, W) = frame.shape[:2]
			fourcc = cv2.VideoWriter_fourcc(*self.args['codec'])

			# Crop off only filename without extension
			output_video = os.path.splitext(os.path.split(self.args["video_src"])[1])[0]
			writer = cv2.VideoWriter(os.path.join(self.path, "{0}.{1}".format(output_video, self.args['video_extension'])), 
				fourcc, self.args['frames_per_second'], (frame.shape[1], frame.shape[0]), True)
		else:
			print("Videofile either does not exist or corrupted")
			return
		vs.release()

		fvs = FileVideoStream(self.args["video_src"]).start()
		time.sleep(1.0)
		fps = FPS().start()
		self.stat.write("Frame time processing (sec) and detection rate:\n")

		for i in tqdm(range(total)):
			start = time.time()
			frame = fvs.read()
			output, det_rate = self.process_image_darknet(frame)
			writer.write(output)
			self.stat.write("Frame #{} - {:.4f}\t{}\n".format(i+1, time.time() - start, det_rate))
			fps.update()

		# Release all pointers and finish the tasks
		fps.stop()
		writer.release()
		cv2.destroyAllWindows()
		fvs.stop()

		print("[INFO] Elasped time: {:.2f}\n".format(fps.elapsed()))
		print("[INFO] Approximate FPS: {:.2f}\n".format(fps.fps()))

		self.stat.write("[INFO] Elasped time: {:.2f}\n".format(fps.elapsed()))
		self.stat.write("[INFO] Approximate FPS: {:.2f}\n".format(fps.fps()))

		# Close stat-file
		self.stat.close()

	# Check for image extension (must contain 'image date' in the source-info)
	def _check_image_extension(self, file):

		try:
			if 'image data' in magic.from_file(file):
				return True
			return False
		except IsADirectoryError:
			return False

	def image_processing(self):

		self.stat.write("Image time processing (sec) and detection rate:\n")
		all_images = os.listdir(self.args['image_src'])
		all_images_l = len(all_images)
		
		if self.args['darknet']:
			self.process_frame = self.process_image_darknet
		elif self.args['caffe']:
			self.process_frame = self.process_frame_caffe

		for i in tqdm(range(all_images_l)):

			start = time.time()

			if not self._check_image_extension(os.path.join(self.args['image_src'], all_images[i])):
				continue

			image = cv2.imread(os.path.join(self.args['image_src'], all_images[i]))
			self.stat.write(os.path.join(self.args['image_src'], all_images[i]))
			self.stat.write('\n')
			processed_image, det_rate = self.process_frame(image)

			for ext in ['jpg', 'png', 'jpeg']:
				try:
					image_name = '{0}_{1}.{2}'.format(all_images[i], self.timestamp, ext)
					break
				except FileNotFoundError:
					pass

			cv2.imwrite(os.path.join(self.path, image_name), processed_image)
			# self.stat.write("{}\t{:.6f}\t{}\n".format(image_name, time.time() - start, det_rate))
		self.stat.close()
			
	def videocapture(self):

		vs = VideoStream(src=self.args['video_cap']).start()

		if self.args['frames_per_second'] == 0:
			self.args['frames_per_second'] = 140
		
		time.sleep(1)
		cap_length = time.time()
		frame_num = 1
		self.stat.write("Frame time processing (sec) and detection rate:\n")

		if self.args['darknet']:
			if self.args['multiprocessing']:
				self.process_frame = self.process_frame_darknet_mp
				p = Process(target=self.classify_frame_darknet)
			else:
				self.process_frame = self.process_image_darknet
		elif self.args['caffe']:
			self.process_frame = self.process_frame_caffe
			p = Process(target=self.classify_frame_caffe)

		fps = FPS().start()
		if self.args['multiprocessing']:
			p.daemon = True
			p.start()

		# With video output file and stat-file
		# Two separate while-loops with the same main code 
		# in order to exclude additional if-statements which decrease FPS
		if self.args['output']:
			out = self.save_video(vs)
			while True:

				start = time.time()
				frame = vs.read()
				output, det_rate = self.process_frame(frame)
				cv2.imshow('output', output)

				# Write frame by frame
				out.write(output)
						
				self.stat.write("[INFO] Frame #{} -\t{:.6f}\t{}\n".format(frame_num, time.time() - start, det_rate))
				if cv2.waitKey(1) & 0xFF == ord('q'):
					break

				frame_num += 1
				fps.update()

			out.release()
			self.stat.write("[INFO] Capture length (sec)\t{:.6f}\n".format(time.time() - cap_length))
			self.stat.write("[INFO] {0} total frames\n".format(frame_num))
		else:
			while True:

				frame = vs.read()
				output, det_rate = self.process_frame(frame)
				cv2.imshow('output', output)
				if cv2.waitKey(1) & 0xFF == ord('q'):
					break

				frame_num += 1
				fps.update()

		# Stop FPS instance and measure FPS
		fps.stop()
		print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
		print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

		vs.stop()
		cv2.destroyAllWindows()
		self.stat.close()

	def start(self):

		# User must choose one algotithm 
		checksum = 0
		if self.args['darknet']:
			checksum += 1
		if self.args['caffe']:
			checksum += 1

		if checksum != 1:
			print("There must be only one model-flag called [d/darknet,c/caffe]")
			return

		# User must choose one mode of operation
		checksum = 0
		if self.args['image_src']:
			checksum += 1
		if self.args['video_src']:
			checksum += 1
		if type(self.args['video_cap']) == type(0):
			checksum += 1

		if checksum != 1:
			print("There must be only one mode-flag called [is/image_src, vs/video_src, vc/video_cap]")
			return

		try:
			if self.define_net_colors_labels_layers():
				pass
			else:
				return
		except AttributeError:
			return

		self.stat = self.save_statistics()

		if self.args["gpu"]:
			# set CUDA as the preferable backend and target
			print("[INFO] setting preferable backend and target to CUDA...")
			try:
				self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
				self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
			except AttributeError:
				self.stat.write("Current version of OpenCV has no support for CUDA\n")
				print("Current version of OpenCV has no support for CUDA")
			except:
				self.stat.write("Could not start CUDA. Starting without GPU\n")
				print("Could not start CUDA. Starting without GPU")

		if self.args['image_src']:
			self.image_processing()
					
		elif self.args['video_src']:
			self.video_processing()

		elif type(self.args['video_cap']) == type(0):
			cap = cv2.VideoCapture(self.args['video_cap'])
			if cap is None or not cap.isOpened():
				self.stat.write("Camera #{0} is not available".format(self.args['video_cap']))
				print("Camera #{0} is not available".format(self.args['video_cap']))
				return
			cap.release()
			self.videocapture()

def main():
		
	instance = Test()
	instance.start()

if __name__ == '__main__':
	main()
